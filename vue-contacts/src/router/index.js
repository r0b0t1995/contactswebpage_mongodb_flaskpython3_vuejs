import { createRouter, createWebHistory } from 'vue-router'
import getContacts from '../contacts/getContacts.vue'
import postContact from '../contacts/postContact.vue'
import getContact from '../contacts/getContact.vue'

const routes = [
  {path: '/', name: 'getContacts', component: getContacts},

  {path: '/postContact', name: 'postContact', component: postContact},

  {path: '/:name/getContact', name: 'getContact', component: getContact},
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
