#!/usr/bin/python3
from decouple import config
from flask import Flask, jsonify, request
from pymongo import MongoClient
from flask_cors import CORS

#class to create connection with mongodb server and CRUD methods
class connection_server:
    # method main and connection with mongo  database
    def __init__(self, mongo_host, mongo_user_name, mongo_password, mongo_database):
        client = MongoClient(mongo_host, username=mongo_user_name, password=mongo_password, authSource=mongo_database)
        data_base = client['robertdb']
        self.collection_contacts = data_base['contacts']

    # return contacts list with all attributes
    def getContacts(self) -> list:
        conn = self.collection_contacts #conn = comunication with the server
        conn = conn.find()
        list_contacts = list()
        for i in conn:
            contacts_dict = {
                'name': i['name'],
                'mobile': i['mobile']
            }
            list_contacts.append(contacts_dict)
        conn.close()
        return list_contacts

    # method that return specific contact
    def getContact(self, contact_name) -> dict:
        conn = self.collection_contacts
        query = {'name': contact_name}
        contact = conn.find_one(query)
        object_contact = {
            'name': contact['name'],
            'mobile': contact['mobile']
        }
        return object_contact

    # method for update a contact in the mongo database
    def patchContact(self, contact_name, object_contact_data) -> str: #update o modify contacts data
        conn = self.collection_contacts
        i = conn.update_one(contact_name, {'$set':object_contact_data})
        if i.modified_count >= 1:
            sms = 'Success to modify contact'
        else:
            sms = 'Error to update contact, try again'
        return sms

    # method for save a contact in the mongo database
    def postContact(self, object_contact_data) -> str: #save a new contact
        conn = self.collection_contacts
        i = conn.insert_one(object_contact_data).inserted_id
        if i is not None:
            sms = 'Success to save contact'
        else:
            sms = 'Error to save contact, try again'
        return sms


    # method for delete a contact in the mongo database
    def deleteContact(self, object_contact_data) -> str: #delete a contact
        conn = self.collection_contacts
        i = conn.delete_one(object_contact_data)
        if i.deleted_count == 1:
            sms = 'Success to delete a contact:'
        else:
            sms = 'Error to delete contact, try again'
        return sms



#section for instace flask and create a connection with VUEJS

# create connection with mongodb server

#---> create file .env with parameters
#HOST_PORT=mongodb://ip_address:port
#USER_NAME=user_name
#PASSWORD=****
#DATABASE=name_database

server = connection_server(config('HOST_PORT'), config('USER_NAME'), config('PASSWORD'), config('DATABASE'))
app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins":"*"}})

# return all contacts
@app.get('/api/contacts')
def getContacts():
    return jsonify(server.getContacts())

# return a contact
@app.get('/api/contact/<string:name>')
def getContact(name):
    return jsonify(server.getContact(contact_name=name))

# save or insert a new contact
@app.post('/api/contact')
def postContact():
    contact_data = {
        'name':request.args['name'],
        'mobile':request.args['mobile']
    }
    if contact_data['mobile'].isnumeric() and len(contact_data['mobile']) < 11 and len(contact_data['name']) < 30:
        return jsonify(server.postContact(contact_data))
    else:
        return jsonify('Error, try again')

# update contact
@app.patch('/api/contact/<string:from_app_name>')
def patchContact(from_app_name):
    contact_name = {
        'name': from_app_name
    }
    contact_data = {
        'name': request.args['name'],
        'mobile': request.args['mobile']
    }
    if contact_data['mobile'].isnumeric() and len(contact_data['mobile']) < 11 and len(contact_data['name']) < 30:
        return jsonify(server.patchContact(contact_name, contact_data))
    else:
        return jsonify('Error, try again')

# delete a contact
@app.delete('/api/contact/<string:from_app_mobile>')
def deleteContact(from_app_mobile):
    contact_data = {
        'mobile': from_app_mobile
    }
    if contact_data['mobile'].isnumeric():
        return jsonify(server.deleteContact(contact_data))
    else:
        return jsonify('Error, try again')



if __name__ == '__main__':
    app.run(port=3000, debug=True)


